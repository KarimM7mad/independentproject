package orgg.independentgrp.independentproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndependentprojApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndependentprojApplication.class, args);
    }

}
