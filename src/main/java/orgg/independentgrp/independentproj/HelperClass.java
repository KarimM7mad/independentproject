package orgg.independentgrp.independentproj;

import org.springframework.util.StringUtils;


public class HelperClass {

    public static String getAppName(String nm) {
        if (!StringUtils.hasText(nm))
            nm = "elDefault app";
        return "I am from another Package &\nMy name is " + nm + "\n";
    }

}
